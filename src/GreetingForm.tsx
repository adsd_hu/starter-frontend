import {useForm} from "@mantine/form";
import Greeting from './Greeting';
import {Button, Group, TextInput} from "@mantine/core";
import {useState} from "react";

interface GreetRequest {
    name: String
}

function GreetingForm() {
    const form= useForm<GreetRequest>({
        initialValues: {
            name: '',
        },
        validate: {
            name: (value) => value ? null : 'Invalid name',
        },
    });
    const [greeter, setGreeter] = useState({
        text: "The greeting will appear here", color: "purple"
    });

    const fetchGreeting = async function (request: GreetRequest): Promise<void> {
        const name = request.name
        console.log("Calling the backend to get a greeting for " + name);

        try {
            const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/greeting/random?name=` + name);
            console.log("Response is ok? " + response.ok + "; Status code " + response.status);

            const json = await response.json();
            console.log("Got a json response: " + JSON.stringify(json));

            if (response.ok) {
                console.log("All is fine");
                setGreeter({...greeter, ...{color: "green", text: json.text}});
            } else {
                console.log("Something went wrong. Got a status " + response.status + " from the server.");
            }
        } catch (ex) {
            if (ex instanceof Error) {
                console.log("Something went wrong. Exception message is '" + ex.message + "'");
            } else {
                console.log("Unknown exception " + ex);
            }
            setGreeter({text: "Error while fetching a random greeting", color: "red"});
        }
    }
    return (
        <form onSubmit={form.onSubmit(fetchGreeting)}>
            <TextInput
                label="Name"
                placeholder="your name"
                {...form.getInputProps('name')}
            />
            <Group position="right" mt="md">
                <Button type="submit">Submit</Button>
            </Group>
            <Greeting text={greeter.text} color={greeter.color} mt="md" />
        </form>
    );
}

export default GreetingForm;