import './App.css';
import {
    AppShell,
    Text,
    Header,
    MantineProvider,
    Navbar,
    Box,
    Title,
} from "@mantine/core";
import GreetingForm from './GreetingForm';


function App() {
    return (
        <MantineProvider withGlobalStyles withNormalizeCSS>
            <AppShell
                padding="md"
                navbar={<Navbar width={{base: 100}} height={500} p="xs">{/* Navbar content */}</Navbar>}
                header={<Header height={60} p="xs"><Title>Starter Project</Title>{/* Header content */}</Header>}
            >
                <Box maw={300} mx="auto">
                    <Text>Here are some things to think about...</Text>
                    <GreetingForm/>
                </Box>
            </AppShell>
        </MantineProvider>
    );
}

export default App;
