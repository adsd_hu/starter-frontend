import { Alert, DefaultProps } from "@mantine/core";

interface GreetProps extends DefaultProps {
    text: string,
    color: string,
}

function Greeting(greetProps: GreetProps & DefaultProps) {
    return (
        <Alert title="Greeting" styles={greetProps.styles} color={greetProps.color}>{greetProps.text}</Alert>
    );
}

export default Greeting;
